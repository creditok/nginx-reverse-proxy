FROM nginx:1.19
RUN mkdir /etc/nginx/templates
COPY ./templates /etc/nginx/templates
STOPSIGNAL SIGTERM
CMD ["nginx","-g","daemon off;"]
ENTRYPOINT ["/docker-entrypoint.sh"]